#! /usr/bin/env python3
import bs4
import feedparser
import pandas as pd
import shelve
import sys
import os

class News:
    def __init__(self,link,title,summary,img_src):
        self.link = link
        self.title = title
        self.summary = summary
        self.img_src = img_src

    def __repr__(self):
        return 'title:{}\nsummary:{}'.format(self.title,self.summary)

if(len(sys.argv) != 3):
    print('usage: ./rss_sample_save.py <dataname> <RSS_URL>')
    sys.exit()

RSS_URL = sys.argv[2] #RSS取得先URL

feeds = feedparser.parse(RSS_URL)

os.makedirs(sys.argv[1],exist_ok=True)

entries = pd.DataFrame(feeds.entries) #これよくわからん
newsList = []
d = shelve.open('{}/mydata_{}'.format(sys.argv[1],sys.argv[1]))#TODO: change

for key, row in entries.iterrows():
    #print(row['summary'])
    #print(row.media_content)
    print('■',end = '')
    img_src = None
    if not pd.isnull(row.media_content):
        img_src = row.media_content[0]['url']

    soup = bs4.BeautifulSoup(row['summary'])
    if soup.select('p') != []:
        summary = soup.select('p')[0].getText() #要約文
        if summary != '\n':
            news = News(row['link'],row['title'],summary,img_src) #ニュースオブジェクト生成
            newsList.append(news)
print('')

d['newslist'] = newsList
d.close()
