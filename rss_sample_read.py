#! /usr/bin/env python3
import bs4
import feedparser
import pandas as pd
import xmlrpc.client
import shelve
import sys
import os

class News:
    def __init__(self,link,title,summary):
        self.link = link
        self.title = title
        self.summary = summary

    def __repr__(self):
        return 'title:{}\nsummary:{}'.format(self.title,self.summary)

if(len(sys.argv) != 2):
    print('usage: ./rss_sample_read.py <dataname>')
    sys.exit()

server = xmlrpc.client.ServerProxy("http://d.hatena.ne.jp/xmlrpc")

d = shelve.open('mydata_{}'.format(sys.argv[1]),flag = 'r')

newsList = d['newslist']

for news in newsList:
    if news.summary != '\n':
        res = server.hatena.setKeywordLink({"body": news.summary, 'mode': 'lite'}) #概要文解析
        wordlist = sorted(res['wordlist'], key=lambda x:x['score'],reverse = True) #概要分をスコアの降順にソート
        blank1 = wordlist[0]['word'] #一つ目の空欄
        blank2 = wordlist[1]['word'] #二つ目の空欄
        replaced_summary = news.summary.replace(blank1,'〇〇〇〇') #置き換え
        replaced_summary = replaced_summary.replace(blank2,'●●●●') #置き換え
        #print(replaced_summary)
        #print(news.summary)
        #print(news.img_src)
        print(news.link)
        print('■',end = '')
print('')
d.close()
