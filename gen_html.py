#! /usr/bin/env python3
import os
import sys

if len(sys.argv) != 3:
    print('usage: ./gen_html.py <name> <URL>')
    sys.exit()

name = sys.argv[1]
url = sys.argv[2]
os.system('./rss_sample_save.py {} {}'.format(name,url))
os.system('./rss_sample_html.py {}'.format(name))
