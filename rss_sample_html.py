#! /usr/bin/env python3
import bs4
import feedparser
import pandas as pd
import xmlrpc.client
import shelve
import os
import sys
import requests

class News:
    def __init__(self,link,title,summary):
        self.link = link
        self.title = title
        self.summary = summary

    def __repr__(self):
        return 'title:{}\nsummary:{}'.format(self.title,self.summary)

def printHeader(link,title):
    file = open(link, "w")
    header = """
    <html>
        <head>
            <title>{}</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        </head>
        <body>
        <div class = "col-12 col-md-6 mx-auto">
    """.format(title)
    file.write(header)

def printFooter(link):
    file = open(link, "a")
    footer = """
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        </body>
    </html>
    """
    file.write(footer)
    file.close()


def generateNewsPage(link,title,contents,backurl):
    h1 = 'News:{}'.format(title)
    printHeader(link,title)



    o = open(link, "a")
    #print(contents)
    if contents == None:
        print(contents)
        print('contents is null')
        sys.exit()

    #o.write('<div class = "col-12 col-md-6 mx-auto">')
    o.write('<div class="jumbotron col-12" style = "margin-top:80px;"><h1 class="display-6">{}</h1><hr class="my-4">'.format(h1))
    for p in contents:
        o.write(p)

    o.write('<div class = "mx-auto" style="width: 200px;"><a href="{}" class="btn btn-primary">Back to top</a></div></div>'.format(backurl))
    #o.write('</div>')
    o.close()

    printFooter(link)

def generateQuiz(link,title,quiz,answer,backurl):
    printHeader(link,title)

    o = open(link, "a")

    o.write('<div class="jumbotron jumbotron-fluid" style = "margin-top:80px;"><div class = "container"><h1>Quiz:</h1>')
    #write contents
    content = """
    <p>{}</p>
    """.format(quiz)
    o.write(content)

    modal = """
    <!-- Button trigger modal -->
<button type="button" class="btn btn-danger float-right" style="width: 200px;" data-toggle="modal" data-target="#exampleModal">
  >> Answer <<
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Answer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    """.format(answer)
    o.write(modal)

    o.write('<div class = "mx-auto" style="width: 200px;"><a href="{}" class="btn btn-primary">Back to top</a></div></div></div>'.format(backurl))
    o.close()

    printFooter(link)

def getContent(url):
    res = requests.get(url)
    res.raise_for_status()
    soup = bs4.BeautifulSoup(res.text, "html.parser")

    title = soup.select('#__r_article_title__')

    if title == []:
        sys.exit()

    #print(title[0].string)

    p_list = soup.article.select('p')

    sentence =[]

    for p in p_list:
        sentence.append(str(p))
        #print(p.string)
        #print('-'*100)

    #print(sentence)
    return sentence

# はてぶキーワード抽出
server = xmlrpc.client.ServerProxy("http://d.hatena.ne.jp/xmlrpc")

if(len(sys.argv) != 2):
    print('usage: ./rss_sample_read.py <dataname>')
    sys.exit()

arg1 = sys.argv[1]

d = shelve.open('{}/mydata_{}'.format(arg1,arg1)) #TODO: change

newsList = d['newslist']
os.makedirs(sys.argv[1],exist_ok=True)
index_name = "index_{}.html".format(arg1)
o = open('{}/{}'.format(arg1,index_name), "w") #TODO: change
header = """
<html>
    <head>
        <title>NEWS&QUIZ!</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    </head>
    <body>
    <div class = "col-12 col-md-9 mx-auto">
    <div class = "row">
    <div class="jumbotron col-12">
      <h1 class="display-3">NEWS & QUIZ!</h1>
      <p class="lead">最新のニュースをチェックして，それに関するクイズに答えてください！</p>
      <hr class="my-4">
      <p>"Check Quiz"ボタンをクリックすると，そのニュースに関するクイズが表示されます．</p>
    </div>


"""

o.write(header)
column_count = 0
#row_div = '<'
#o.write()
os.makedirs('{}/article'.format(arg1),exist_ok = True)
os.makedirs('{}/quiz'.format(arg1),exist_ok = True)
for news in newsList:
    if news.summary != '\n':
        #クイズ作成のためのキーワード抽出
        res = server.hatena.setKeywordLink({"body": news.summary, 'mode': 'lite'}) #概要文解析
        wordlist = sorted(res['wordlist'], key=lambda x:x['score'],reverse = True) #概要分をスコアの降順にソート
        blank1 = wordlist[0]['word'] #一つ目の空欄
        blank2 = wordlist[1]['word'] #二つ目の空欄
        replaced_summary = news.summary.replace(blank1,'〇'*len(blank1)) #置き換え
        answer_summary = news.summary.replace(blank1,'<strong>{}</strong>'.format(blank1))
        replaced_summary = replaced_summary.replace(blank2,'●'*len(blank2)) #置き換え
        answer_summary = answer_summary.replace(blank2,'<strong>{}</strong>'.format(blank2))

        #generate News Page
        # link の basename はじめの15文字をファイル名に採用
        news_content_link = 'article/{}.html'.format(os.path.basename(news.link)[:15])#TODO: change
        news_quiz_link = 'quiz/{}.html'.format(os.path.basename(news.link)[:15])# TODO: change
        img_src = news.img_src
        if img_src == None:
            img_src = 'https://placehold.jp/3d4070/ffffff/200x200.png?text=no%20image'
        news_thumbnails = """
        <a href = "{}">
        <div class="card col-6 col-lg-4" style="width: 20rem;">
          <img class="card-img-top" src="{}" alt="Card image cap">
          <div class="card-body">
            <h4 class="card-title">{}</h4>
            <a href="{}" class="btn btn-primary">Check Quiz</a>
          </div>
        </div>
        </a>
        """.format(news_content_link,img_src,news.title,news_quiz_link)
        #o.write('   <p><a href = "{}">{}\n</a></p>\n'.format(news_content_link,news.title))
        o.write(news_thumbnails)
        #print(news.link)
        news_sentence = getContent(news.link)
        #print(getContent(news.link))
        generateNewsPage('{}/{}'.format(arg1,news_content_link),news.title,news_sentence,'../{}'.format(index_name))

        #generate Quiz Page

        #o.write('   <p><a href = "{}">{}\n</a></p>\n'.format(news_quiz_link,'Quiz'))
        generateQuiz('{}/{}'.format(arg1,news_quiz_link),news.title,replaced_summary,answer_summary,'../{}'.format(index_name))
        print('■',end = '')

print('')

footer = """
    </div>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>
"""
o.write(footer)
o.close()
d.close()
