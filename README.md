# README

## DOWNLOAD

```
$ git clone https://kaito___@bitbucket.org/kaito___/rssreader.git
```

## EXECUTE

```
$ pip install feedparser
$ ./gen_html.py <data_name> <URL>
```

`<URL>` can be selected from list [zakzak](http://www.zakzak.co.jp/rss/index.html)
